import React, {Component} from 'react'

export default class Test extends Component{
    // This is a e.g. of react life-cycle
    
    //life-cycle: mount
    constructor(props){
        super(props)
        console.log('constructor')
        this.state = {
            time : new Date()
        }
    }

    tick(){
        this.setState({
            time: new Date()
        })
    }
    
    componentWillMount(){
        console.log('The component is about to mount/set')
        this.timeId = setInterval(()=>this.tick(),1000)

    }
    
    componentDidMount(){
        console.log('The component has already mounted/setted')
    }

    //life-cycle: update
    componentWillReceiveProps(){
        console.log('The component will receive props')
    }

    // static getDerivedStateFromProps

    //
    shouldComponentUpdate(nextProps, nextState){
        console.log('Should the component update?')
        console.log(nextState)
        if(nextState.time.getSeconds() % 2 === 0)
        {
            return true
        } else {
            return false
        }
    }

    componentWillUpdate(){
        console.log('The component will update')
    }

    // getSnapshotBeforeUpdate(){
    //     console.log('Get snapshot before updateß')
    // }

    componentDidUpdate(){
        console.log('The component has updated')
    }

    //life-cycle: unmount
    componentWillUnmount(){
        console.log('The component will unmount')
        clearInterval(this.timeId)
    }
    render(){
        console.log('render')
        return(
            <div style={{border : 'solid black 1px'}}>
                <p>Test</p>
                <p>{this.state.time.getSeconds()}</p>
                <button type='button' onClick={()=>this.setState({})}>Set state</button>
                <button type='button' onClick={()=>this.forceUpdate()}>Force Update</button>
            </div>
        )
    }
}

//A component has two ways to update: setState() / forceUpdate()
//A component will be updated when its parent component updated
