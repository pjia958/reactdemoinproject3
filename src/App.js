import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';

import Test from './Test'
import DemoN from './DemoNewLifeCycle'
import PropTypes from 'prop-types'

class Title extends Component {
  static defaultProps = {
    title : "This is the default title"
  }
  static propTypes = {
    title : PropTypes.string 
  }

  render(){
    return(
    <div>{this.props.title}</div>
    )
  }
}

// Title.defaultProps = {
//   title: 'default title from outside'
// }

class App extends Component {
  constructor(props){
    super(props)
    this.state = {
      isRenderTest : true
    }
  }
  render() {
    return (
      <div>
        <Title title={2}></Title>
        {this.state.isRenderTest ? <DemoN></DemoN> : 'not going to render Test component'}
        <p>App</p>
        <button type="button" onClick={()=>this.setState({})}>App setState</button>
        <button tyoe="button" onClick={()=>{this.setState({isRenderTest : !this.state.isRenderTest})}}>Switch the isRenderTest</button>
      </div>
    );
  }
}

export default App;
