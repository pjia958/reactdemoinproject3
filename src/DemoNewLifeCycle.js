import React , {Component} from 'react'

export default class DemoN extends Component{
    constructor(props){
        super(props)
        this.state = {
            time : new Date()
        }
        this.timeId = setInterval(()=>this.tick(), 1000)
    }

    tick(){
        this.setState({
            time : new Date()
        })
    }

    static getDerivedStateFromProps() {
        // The return of this method will be added into state
        // if return null, it means there's no change to state
        console.log('New static function in life cycle')
        return null
    }

    getSnapshotBeforeUpdate(){
        console.log('get snapshot')
        return 'from get snapshot' //this is info in componentDidUpdate()
    }
    
    render(){
        console.log('render', this.state)
        return(
            <div>
                <p>Demo</p>
                {/* <button type='button' onClick={()=>this.setState({})}>setState</button> */}
                {this.state.time.getSeconds()}
                </div>
        
        )
    }

    componentDidMount(){
        console.log('component did mount')
    }
    shouldComponentUpdate(){
        console.log('should component update')
        return true
    }
    componentDidUpdate(preprp,prstt,info){
        console.log('componentDidUpdate')
        console.log(info)
    }
    componentWillUnmount(){
        console.log('componentWillUnmount')
        clearInterval(this.timeId)
    }
}